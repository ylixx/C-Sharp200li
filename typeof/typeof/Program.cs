﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @typeof
{
    class Program
    {
        static void Main(string[] args)
        {
            Type t=typeof(int);
            Console.WriteLine(t.Namespace);
            Console.WriteLine(t.FullName);
            Console.WriteLine(t.Name);
            int c = t.GetMethods().Length;
            foreach (var mi in t.GetMethods()) 
            {
                Console.WriteLine(mi.Name);
            }
            Console.WriteLine(c);
            Console.ReadKey();
        }
    }
}
